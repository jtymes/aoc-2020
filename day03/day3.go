package main

import (
	"aoc-2020/aocutil"
	"fmt"
)

func main() {
	input := aocutil.SlurpFile("./day3/input.txt")
	countTrees1(input)
	countTrees2(input)
}

func countTrees1(input []string) {
	treeCount := countTrees(input, 3, 1)
	fmt.Printf("Count 1: %d\n", treeCount)
}

func countTrees2(input []string) {
	slopesToCheck := [][]int{
		{1, 1},
		{3, 1},
		{5, 1},
		{7, 1},
		{1, 2},
	}
	total := 1
	for _, slope := range slopesToCheck {
		total *= countTrees(input, slope[0], slope[1])
	}
	fmt.Printf("Count 2: %d\n", total)
}

func countTrees(input []string, right int, down int) int {
	depth := len(input)
	rowWidth := len(input[0])
	x := 0
	treeCount := 0
	for d := 0; d < depth-down; d += down {
		r := d + down
		if x+right < rowWidth {
			x += right
		} else {
			x = (x + right) - rowWidth
		}
		if input[r][x] == '#' {
			treeCount++
		}
	}
	return treeCount
}

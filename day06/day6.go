package main

import (
	"aoc-2020/aocutil"
	"fmt"
)

func main() {
	input := aocutil.SlurpFile("./day6/input.txt")

	countAnswers(input)
	countAnswers2(input)
}

func countAnswers(input []string) {
	seenAnswers := make(map[string]bool)
	count := 0
	for _, row := range input {
		if row == "" {
			for range seenAnswers {
				count++
			}
			seenAnswers = make(map[string]bool)
		} else {
			for _, r := range row {
				seenAnswers[string(r)] = true
			}
		}
	}
	for range seenAnswers {
		count++
	}
	fmt.Println(count)
}

func countAnswers2(input []string) {
	seenAnswers := make(map[string]int)
	count := 0
	peopleInGroup := 0
	for _, row := range input {
		if row == "" {
			for _, v := range seenAnswers {
				if v == peopleInGroup {
					count++
				}
			}
			seenAnswers = make(map[string]int)
			peopleInGroup = 0
		} else {
			peopleInGroup++
			for _, r := range row {
				seenAnswers[string(r)]++
			}
		}
	}
	for _, v := range seenAnswers {
		if v == peopleInGroup {
			count++
		}
	}
	fmt.Println(count)
}

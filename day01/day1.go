package main

import (
	"aoc-2020/aocutil"
	"strconv"
)

func main() {
	input := aocutil.SlurpFile("./day1/input.txt")
	calculate(input)
}

func calculate(input []string) {
	var nums []int
	for _, n := range input {
		i, _ := strconv.Atoi(n)
		nums = append(nums, i)
	}

	for i := 0; i < len(nums); i++ {
		for j := i + 1; j < len(nums); j++ {
			for k := j + 1; k < len(nums); k++ {
				if nums[i]+nums[j]+nums[k] == 2020 {
					println(nums[i] * nums[j] * nums[k])
					break
				}
			}
		}
	}
}

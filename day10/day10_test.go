package main

import (
	"aoc-2020/aocutil"
	"testing"
)

var testDataPart1 = []struct {
	file           string
	expectedNumber int
}{
	{"./sample.txt", 35},
	{"./sample2.txt", 220},
	{"./input.txt", 2059},
}

func TestJoltage(t *testing.T) {
	for _, td := range testDataPart1 {
		t.Run(td.file, func(t *testing.T) {
			input := aocutil.SlurpFileToI(td.file)
			expectedNumber := td.expectedNumber

			invalidNumber := joltage(input)

			if invalidNumber != expectedNumber {
				t.Errorf("Expected %d, got %d", expectedNumber, invalidNumber)
			}
		})
	}
}

var testDataPart2 = []struct {
	file           string
	expectedNumber int64
}{
	{"./sample.txt", 8},
	{"./sample2.txt", 19208},
	//{"./input.txt", 2059},
}

func TestJoltage2(t *testing.T) {
	for _, td := range testDataPart2 {
		t.Run(td.file, func(t *testing.T) {
			input := aocutil.SlurpFileToI(td.file)
			expectedNumber := td.expectedNumber

			invalidNumber := joltage2(input)

			if invalidNumber != expectedNumber {
				t.Errorf("Expected %d, got %d", expectedNumber, invalidNumber)
			}
		})
	}
}

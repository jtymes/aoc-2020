package main

import (
	"sort"
)

func main() {

}

func joltage(input []int) int {
	sort.Ints(input)
	highestJoltage := input[len(input)-1]
	currentJoltage := 0
	joltages := make(map[int]bool)
	joltages[currentJoltage] = true
	for _, j := range input {
		joltages[j] = true
	}
	increments := []int{1, 2, 3}
	distributions := make(map[int]int)

	for {
		if currentJoltage == highestJoltage {
			break
		}
		for _, inc := range increments {
			_, ok := joltages[currentJoltage+inc]
			if ok {
				distributions[inc]++
				currentJoltage = currentJoltage + inc
				break
			}
		}
	}
	distributions[3]++

	return distributions[1] * distributions[3]
}

func joltage2(input []int) int64 {
	return 0
}

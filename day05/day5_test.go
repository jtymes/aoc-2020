package main

import (
	"testing"
)

func TestDecode(t *testing.T) {
	expectedRow, expectedColumn := 44, 5
	passport := "FBFBBFFRLR"
	row, column := Decode(passport[:7], passport[7:])
	if row != expectedRow || column != expectedColumn {
		t.Fatalf("Expected col %d, got %d. Expected column %d, got %d", expectedRow, row, expectedColumn, column)
	}
}

var rowTestData = []struct {
	row      string
	expected int
}{
	{"F", 0},
	{"B", 1},
	{"FBFBBFF", 44},
}

func TestDecodeRow(t *testing.T) {
	decodeRow := decoder("F")
	for _, tt := range rowTestData {
		t.Run(tt.row, func(t *testing.T) {
			row := tt.row
			rows := identitySlice(len(row))
			actual := decodeRow(row, rows)
			if actual != tt.expected {
				t.Errorf("Expected %d but got %d", tt.expected, actual)
			}
		})
	}
}

var colTestData = []struct {
	col      string
	expected int
}{
	{"L", 0},
	{"R", 1},
	{"RLR", 5},
}

func TestDecodeCol(t *testing.T) {
	decodeCol := decoder("L")
	for _, tt := range colTestData {
		t.Run(tt.col, func(t *testing.T) {
			col := tt.col
			cols := identitySlice(len(col))
			actual := decodeCol(col, cols)
			if actual != tt.expected {
				t.Errorf("Expected %d but got %d", tt.expected, actual)
			}
		})
	}
}

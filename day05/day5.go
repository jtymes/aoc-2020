package main

import (
	"aoc-2020/aocutil"
	"fmt"
	"math"
	"sort"
)

func main() {
	input := aocutil.SlurpFile("./day5/input.txt")

	seats := make([]int, len(input))
	for i, passport := range input {
		row, col := Decode(passport[:7], passport[7:])
		seats[i] = row*8 + col
	}
	sort.Ints(seats)
	fmt.Printf("Highest: %d\n", seats[len(seats)-1])
	for i := 0; i < len(seats)-1; i++ {
		if seats[i+1]-seats[i] != 1 {
			fmt.Printf("Your seat is %d\n", seats[i]+1)
			break
		}
	}
}

func Decode(passportRow, passportCol string) (int, int) {
	rowLen := len(passportRow)
	rows := identitySlice(rowLen)

	decodeRow := decoder("F")
	row := decodeRow(passportRow, rows)

	colLen := len(passportCol)
	cols := identitySlice(colLen)

	decodeCol := decoder("L")
	col := decodeCol(passportCol, cols)

	return row, col
}

func identitySlice(n int) []int {
	s := make([]int, int(math.Pow(2, float64(n))))
	for i := 0; i < len(s); i++ {
		s[i] = i
	}
	return s
}

type decoderFn func(p string, s []int) int

func decoder(front string) decoderFn {
	var decode decoderFn
	decode = func(p string, s []int) int {
		if len(p) == 1 {
			if p == front {
				return s[0]
			} else {
				return s[1]
			}
		}
		first := string(p[0])
		rowLen := len(s) / 2
		if first == front {
			return decode(p[1:], s[:rowLen])
		} else {
			return decode(p[1:], s[rowLen:])
		}
	}
	return decode
}

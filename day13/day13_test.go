package main

import (
	"aoc-2020/aocutil"
	"testing"
)

var testDataPart1 = []struct {
	file           string
	expectedNumber int
}{
	{"./sample.txt", 295},
	{"./input.txt", 6568},
}

func TestBus(t *testing.T) {
	for _, td := range testDataPart1 {
		t.Run(td.file, func(t *testing.T) {
			input := aocutil.SlurpFile(td.file)
			expectedNumber := td.expectedNumber

			invalidNumber := bus(input)

			if invalidNumber != expectedNumber {
				t.Errorf("Expected %d, got %d", expectedNumber, invalidNumber)
			}
		})
	}
}

var testDataPart2 = []struct {
	input          string
	expectedNumber int
}{
	{"7,13,x,x,59,x,31,19", 1068781},
	{"17,x,13,19", 3417},
	{"67,7,59,61", 754018},
	{"67,x,7,59,61", 779210},
	{"67,7,x,59,61", 1261476},
	{"1789,37,47,1889", 1202161486},
	{"19,x,x,x,x,x,x,x,x,41,x,x,x,37,x,x,x,x,x,821,x,x,x,x,x,x,x,x,x,x,x,x,13,x,x,x,17,x,x,x,x,x,x,x,x,x,x,x,29,x,463,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,23", 554865447501099},
}

func TestBus2(t *testing.T) {
	for _, td := range testDataPart2 {
		t.Run(td.input, func(t *testing.T) {
			//input := aocutil.SlurpFile(td.file)
			expectedNumber := td.expectedNumber

			invalidNumber := bus2(td.input)

			if invalidNumber != expectedNumber {
				t.Errorf("Expected %d, got %d", expectedNumber, invalidNumber)
			}
		})
	}
}

package main

import (
	"fmt"
	"math"
	"strconv"
	"strings"
)

func main() {
}

func bus(input []string) int {
	time, _ := strconv.Atoi(input[0])
	buses := strings.Split(input[1], ",")
	lowest := math.MaxInt32
	lowestBusId := 0
	for _, bus := range buses {
		if bus != "x" {
			busId, _ := strconv.Atoi(bus)
			for i, found := time, false; !found; i++ {
				if i%busId == 0 {
					found = true
					if i-time < lowest {
						lowest = i - time
						lowestBusId = busId
					}
				}
			}
		}
	}
	return lowest * lowestBusId
}

func bus2Brute(input string) int64 {
	buses := strings.Split(input, ",")
	busOffsets := make(map[int64]int64)
	busIds := []int64{}
	for offset, bus := range buses {
		if bus != "x" {
			busId, _ := strconv.Atoi(bus)
			busIds = append(busIds, int64(busId))
			busOffsets[int64(busId)] = int64(offset)
		}
	}
	fmt.Println(busIds)
	fmt.Println(busOffsets)

	var i int64
	for found := false; !found; i = i + busIds[0] {
		found = true
		for busId, offset := range busOffsets {
			if (i+offset)%busId != 0 {
				//fmt.Println("Nope")
				found = false
				break
			}
		}
	}
	return i - busIds[0]
}

// I lifted this from someone else. left my brute force solution above.
func bus2(lines string) int {
	buses := map[int]int{}
	for idx, item := range strings.Split(lines, ",") {
		if item == "x" {
			continue
		}
		i, _ := strconv.Atoi(item)
		buses[idx] = i
	}

	t := buses[0]
	delta := buses[0]

	for dt, bus := range buses {
		for {
			if (t+dt)%bus == 0 {
				break
			}
			t += delta
		}
		delta = lcm(delta, bus)
	}

	return t
}

func lcm(a, b int) int {
	gcd := func(a, b int) int {
		for b != 0 {
			t := b
			b = a % b
			a = t
		}
		return a
	}
	return a * b / gcd(a, b)
}

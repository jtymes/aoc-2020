package main

import (
	"aoc-2020/aocutil"
	"testing"
)

var testDataPart1 = []struct {
	file           string
	expectedNumber int
}{
	{"./sample.txt", 37},
	{"./input.txt", 2406},
}

func TestSeats(t *testing.T) {
	for _, td := range testDataPart1 {
		t.Run(td.file, func(t *testing.T) {
			input := aocutil.SlurpFile(td.file)
			expectedNumber := td.expectedNumber

			invalidNumber := seats(input)

			if invalidNumber != expectedNumber {
				t.Errorf("Expected %d, got %d", expectedNumber, invalidNumber)
			}
		})
	}
}

var testDataPart2 = []struct {
	file           string
	expectedNumber int
}{
	{"./sample.txt", 26},
	{"./input.txt", 2149},
}

func TestSeats2(t *testing.T) {
	for _, td := range testDataPart2 {
		t.Run(td.file, func(t *testing.T) {
			input := aocutil.SlurpFile(td.file)
			expectedNumber := td.expectedNumber

			invalidNumber := seats2(input)

			if invalidNumber != expectedNumber {
				t.Errorf("Expected %d, got %d", expectedNumber, invalidNumber)
			}
		})
	}
}

package main

import (
	"strings"
)

func main() {

}

func seats(input []string) int {
	seats := make([][]string, 0, len(input))
	for _, v := range input {
		seats = append(seats, strings.Split(v, ""))
	}
	seatsInRow := len(seats[0])
	rowsInPlane := len(input)

	madeChange := true
	for madeChange {
		madeChange = false
		newSeats := make([][]string, 0, rowsInPlane)
		for ri, row := range seats {
			newRow := make([]string, 0, seatsInRow)
			for si, seat := range row {
				//fmt.Printf("Processing '%s', with coordinates (%d, %d)\n", seat, si, ri)
				if seat == "." {
					newRow = append(newRow, seat)
					continue
				}
				occupiedSeats := 0
				for x := si - 1; x <= si+1; x++ {
					for y := ri - 1; y <= ri+1; y++ {
						if x < 0 || y < 0 || x >= seatsInRow || y >= rowsInPlane {
							//fmt.Print("-")
						} else {
							//fmt.Print(seats[y][x])
							if x == si && y == ri {
								continue
							}
							if seats[y][x] == "#" {
								occupiedSeats++
							}
						}
					}
					//fmt.Print("\n")
				}
				//fmt.Printf("Occupied: %d\n", occupiedSeats)
				//fmt.Println("------")
				if seat == "L" && occupiedSeats == 0 {
					newRow = append(newRow, "#")
					madeChange = true
				} else if seat == "#" && occupiedSeats >= 4 {
					newRow = append(newRow, "L")
					madeChange = true
				} else {
					newRow = append(newRow, seat)
				}
				//fmt.Println(occupiedSeats)
			}
			newSeats = append(newSeats, newRow)
		}
		// for printing the layouts
		//for i, r := range newSeats {
		//	for j, _ := range r {
		//		fmt.Print(newSeats[i][j])
		//	}
		//	fmt.Print("\n")
		//}
		//fmt.Println(strings.Repeat("-", seatsInRow))
		seats = newSeats
	}
	occupiedSeats := 0
	for _, r := range seats {
		for _, s := range r {
			if s == "#" {
				occupiedSeats++
			}
		}
	}
	return occupiedSeats
}

func seats2(input []string) int {
	seats := make([][]string, 0, len(input))
	for _, v := range input {
		seats = append(seats, strings.Split(v, ""))
	}
	seatsInRow := len(seats[0])
	rowsInPlane := len(input)

	madeChange := true
	for madeChange {
		madeChange = false
		newSeats := make([][]string, 0, rowsInPlane)
		for ri, row := range seats {
			newRow := make([]string, 0, seatsInRow)
			for si, seat := range row {
				//fmt.Printf("Processing '%s', with coordinates (%d, %d)\n", seat, si, ri)
				if seat == "." {
					newRow = append(newRow, seat)
					continue
				}
				type slope struct {
					run  int
					rise int
				}
				slopes := []slope{
					{-1, 0},
					{-1, -1},
					{0, -1},
					{1, -1},
					{1, 0},
					{1, 1},
					{0, 1},
					{-1, 1},
				}
				occupiedSeats := 0
			Slope:
				for _, s := range slopes {
					for x, y := si, ri; x < seatsInRow && x >= 0 && y < rowsInPlane && y >= 0; x, y = x+s.run, y+s.rise {
						if x == si && y == ri {
							continue
						}

						if seats[y][x] == "#" {
							//fmt.Printf("(%d,%d) %s %#v\n",x,y,seats[y][x],s)
							occupiedSeats++
							continue Slope
						} else if seats[y][x] == "L" {
							continue Slope
						}
					}
				}

				//fmt.Printf("Occupied: %d\n", occupiedSeats)
				//fmt.Println("------")
				if seat == "L" && occupiedSeats == 0 {
					newRow = append(newRow, "#")
					madeChange = true
				} else if seat == "#" && occupiedSeats >= 5 {
					newRow = append(newRow, "L")
					madeChange = true
				} else {
					newRow = append(newRow, seat)
				}
			}
			newSeats = append(newSeats, newRow)
		}
		// for printing the layouts
		//for i, r := range newSeats {
		//	for j, _ := range r {
		//		fmt.Print(newSeats[i][j])
		//	}
		//	fmt.Print("\n")
		//}
		//fmt.Println(strings.Repeat("-", seatsInRow))
		seats = newSeats
	}
	occupiedSeats := 0
	for _, r := range seats {
		for _, s := range r {
			if s == "#" {
				occupiedSeats++
			}
		}
	}
	return occupiedSeats
}

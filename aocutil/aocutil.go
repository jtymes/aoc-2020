package aocutil

import (
	"bufio"
	"log"
	"os"
	"strconv"
)

func SlurpFile(path string) []string {
	openFile, e := os.Open(path)
	if e != nil {
		log.Fatal(e)
	}
	defer openFile.Close()
	scanner := bufio.NewScanner(openFile)
	var lines []string
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines
}

func SlurpFileToI(path string) []int {
	openFile, e := os.Open(path)
	if e != nil {
		log.Fatal(e)
	}
	defer openFile.Close()
	scanner := bufio.NewScanner(openFile)
	var lines []int
	for scanner.Scan() {
		line, err := strconv.Atoi(scanner.Text())
		if err != nil {
			continue
		}
		lines = append(lines, line)
	}
	return lines
}

package main

import (
	"aoc-2020/aocutil"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

var debug = false

func main() {
	input := aocutil.SlurpFile("./day4/input.txt")
	countValidPassports1(input)
}

func countValidPassports1(input []string) {
	passport := ""
	reqFields := []string{"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"}
	valid := 0
	for i, r := range input {
		if r == "" || i == len(input)-1 {
			if i == len(input)-1 {
				passport += r
			}
			if debug {
				fmt.Println(passport)
			}
			missingField := false
			for _, field := range reqFields {
				if !strings.Contains(passport, field+":") {
					missingField = true
				}
			}
			if !missingField {
				fieldsMap := make(map[string]string)
				fields := strings.Split(strings.TrimSpace(passport), " ")
				for _, f := range fields {
					split := strings.Split(strings.TrimSpace(f), ":")
					k := split[0]
					v := split[1]
					fieldsMap[k] = v
				}
				if validateFields(fieldsMap) {
					valid++
				}
			}
			passport = ""
		} else {
			passport += r + " "
		}
	}
	fmt.Printf("%d\n", valid)
}

func validateFields(fieldsMap map[string]string) bool {
	if !validateBirthYear(fieldsMap["byr"]) ||
		!validateIssueYear(fieldsMap["iyr"]) ||
		!validateExpirationYear(fieldsMap["eyr"]) ||
		!validateHeight(fieldsMap["hgt"]) ||
		!validateHairColor(fieldsMap["hcl"]) ||
		!validateEyeColor(fieldsMap["ecl"]) ||
		!validatePid(fieldsMap["pid"]) {
		return false
	}
	return true
}

func validatePid(pid string) bool {
	return regexp.MustCompile("^[0-9]{9}$").MatchString(pid)
}

func validateEyeColor(ecl string) bool {
	return regexp.MustCompile("(amb|blu|brn|gry|grn|hzl|oth)").MatchString(ecl)
}

func validateHairColor(hcl string) bool {
	return regexp.MustCompile("#[0-9a-f]{6}").MatchString(hcl)
}

func validateHeight(hgt string) bool {
	heightPattern := regexp.MustCompile("([0-9]{2,3})(in|cm)")
	if heightPattern.MatchString(hgt) {
		matched := heightPattern.FindAllStringSubmatch(hgt, -1)

		hgt, err := strconv.Atoi(matched[0][1])
		if err != nil {
			return false
		}

		unit := matched[0][2]
		if unit == "in" {
			if hgt < 59 || hgt > 76 {
				return false
			}
		} else if unit == "cm" {
			if hgt < 150 || hgt > 193 {
				return false
			}
		} else {
			return false
		}
	} else {
		return false
	}
	return true
}

func validateBirthYear(byr string) bool {
	return validateYearRange(byr, 1920, 2002)
}

func validateIssueYear(iyr string) bool {
	return validateYearRange(iyr, 2010, 2020)
}

func validateExpirationYear(eyr string) bool {
	return validateYearRange(eyr, 2020, 2030)
}

func validateYearRange(val string, min, max int) bool {
	yearPattern := regexp.MustCompile("[0-9]{4}")
	if yearPattern.MatchString(val) {
		parsedYear, err := strconv.Atoi(yearPattern.FindString(val))
		if err != nil || parsedYear < min || parsedYear > max {
			return false
		}
	} else {
		return false
	}
	return true
}

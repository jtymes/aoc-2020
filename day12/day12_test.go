package main

import (
	"aoc-2020/aocutil"
	"testing"
)

var testDataPart1 = []struct {
	file           string
	expectedNumber int
}{
	{"./sample.txt", 25},
	{"./input.txt", 938},
}

func TestPosition(t *testing.T) {
	for _, td := range testDataPart1 {
		t.Run(td.file, func(t *testing.T) {
			input := aocutil.SlurpFile(td.file)
			expectedNumber := td.expectedNumber

			invalidNumber := position(input)

			if invalidNumber != expectedNumber {
				t.Errorf("Expected %d, got %d", expectedNumber, invalidNumber)
			}
		})
	}
}

var testDataPart2 = []struct {
	file           string
	expectedNumber int
}{
	{"./sample.txt", 286},
	{"./input.txt", 54404},
}

func TestSeats2(t *testing.T) {
	for _, td := range testDataPart2 {
		t.Run(td.file, func(t *testing.T) {
			input := aocutil.SlurpFile(td.file)
			expectedNumber := td.expectedNumber

			invalidNumber := position2(input)

			if invalidNumber != expectedNumber {
				t.Errorf("Expected %d, got %d", expectedNumber, invalidNumber)
			}
		})
	}
}

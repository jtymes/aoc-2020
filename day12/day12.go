package main

import (
	"fmt"
	"math"
	"strconv"
)

func main() {
}

type point struct {
	x int
	y int
}
type ship struct {
	facing string
	x      int
	y      int
}

func position(input []string) int {
	myShip := ship{
		"E",
		0,
		0,
	}

	for _, instruction := range input {
		action := string(instruction[0])
		value, err := strconv.Atoi(instruction[1:])
		if err != nil {
			fmt.Println("this won't happen")
		}
		switch action {
		case "F":
			switch myShip.facing {
			case "E":
				myShip.x += value
			case "W":
				myShip.x -= value
			case "N":
				myShip.y += value
			case "S":
				myShip.y -= value
			}
		case "E":
			myShip.x += value
		case "W":
			myShip.x -= value
		case "N":
			myShip.y += value
		case "S":
			myShip.y -= value
		case "L":
			num := value / 90
			for i := 0; i < num; i++ {
				if myShip.facing == "E" {
					myShip.facing = "N"
				} else if myShip.facing == "N" {
					myShip.facing = "W"
				} else if myShip.facing == "W" {
					myShip.facing = "S"
				} else if myShip.facing == "S" {
					myShip.facing = "E"
				}
			}
		case "R":
			num := value / 90
			for i := 0; i < num; i++ {
				if myShip.facing == "E" {
					myShip.facing = "S"
				} else if myShip.facing == "N" {
					myShip.facing = "E"
				} else if myShip.facing == "W" {
					myShip.facing = "N"
				} else if myShip.facing == "S" {
					myShip.facing = "W"
				}
			}
		}
	}
	//fmt.Printf("%#v\n", myShip)
	return int(math.Abs(float64(myShip.x)) + math.Abs(float64(myShip.y)))
}

func position2(input []string) int {
	myShip := ship{
		"E",
		0,
		0,
	}
	wp := point{
		10,
		1,
	}

	for _, instruction := range input {
		action := string(instruction[0])
		value, err := strconv.Atoi(instruction[1:])
		if err != nil {
			fmt.Println("this won't happen")
		}
		switch action {
		case "F":
			myShip.x += wp.x * value
			myShip.y += wp.y * value
		case "E":
			wp.x += value
		case "W":
			wp.x -= value
		case "N":
			wp.y += value
		case "S":
			wp.y -= value
		case "L":
			num := value / 90
			for i := 0; i < num; i++ {
				wp.x, wp.y = -wp.y, wp.x
			}
		case "R":
			num := value / 90
			for i := 0; i < num; i++ {
				wp.x, wp.y = wp.y, -wp.x
			}
		}
		//fmt.Printf("%#v %#v\n", myShip, wp)
	}
	return int(math.Abs(float64(myShip.x)) + math.Abs(float64(myShip.y)))
}

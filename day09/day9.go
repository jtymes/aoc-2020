package main

func main() {

}

func processXmas(preambleLen int, input []int) int {
Outer:
	for i := preambleLen; i < len(input); i++ {
		preamble := input[i-preambleLen : i]
		curr := input[i]
		for _, j := range preamble {
			for _, k := range preamble {
				if j != k && j+k == curr {
					continue Outer
				}
			}
		}
		return curr
	}
	return 0
}

func processXmas2(targetNumber int, input []int) int {
	startIdx := 0
	var set []int
Outer:
	for {
		set = []int{}
		for i := startIdx; i < len(input)-1; i++ {
			set = append(set, input[i])

			sum := 0
			for _, v := range set {
				sum += v
			}
			if sum == targetNumber && len(set) >= 2 {
				break Outer
			}
			if sum > targetNumber {
				startIdx++
				continue Outer
			}

		}

	}
	min := set[0]
	for _, v := range set {
		if v < min {
			min = v
		}
	}

	max := set[0]
	for _, v := range set {
		if v > max {
			max = v
		}
	}

	return min + max
}

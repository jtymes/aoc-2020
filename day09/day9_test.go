package main

import (
	"aoc-2020/aocutil"
	"testing"
)

var testDataPart1 = []struct {
	file           string
	preambleLen    int
	expectedNumber int
}{
	{"./sample.txt", 5, 127},
	{"./input.txt", 25, 70639851},
}

func TestProcessXmas(t *testing.T) {
	for _, td := range testDataPart1 {
		t.Run(td.file, func(t *testing.T) {
			input := aocutil.SlurpFileToI(td.file)
			preambleLen := td.preambleLen
			expectedNumber := td.expectedNumber

			invalidNumber := processXmas(preambleLen, input)

			if invalidNumber != expectedNumber {
				t.Errorf("Expected %d, got %d", expectedNumber, invalidNumber)
			}
		})
	}
}

var testDataPart2 = []struct {
	file         string
	targetNumber int
	expectedSum  int
}{
	{"./sample.txt", 127, 62},
	{"./input.txt", 70639851, 8249240},
}

func TestProcessXmas2(t *testing.T) {
	for _, td := range testDataPart2 {
		t.Run(td.file, func(t *testing.T) {
			input := aocutil.SlurpFileToI(td.file)
			targetNumber := td.targetNumber
			expectedSum := td.expectedSum

			result := processXmas2(targetNumber, input)

			if result != expectedSum {
				t.Errorf("Expected %d, got %d", expectedSum, result)
			}
		})
	}
}

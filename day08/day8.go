package main

import (
	"aoc-2020/aocutil"
	"fmt"
	"strconv"
	"strings"
)

func main() {
	input := aocutil.SlurpFile("./day8/input.txt")

	followInstructions(input)
	followInstructions2(input)
}

func followInstructions(input []string) {
	seen := make(map[int]bool, len(input))
	accumulator := 0
	i := 0
	for {
		if seen[i] {
			break
		}
		seen[i] = true
		instruction := input[i]

		spaceIndex := strings.Index(instruction, " ")
		call := instruction[:spaceIndex]
		incOrDec := instruction[spaceIndex+1 : spaceIndex+2]
		amount, err := strconv.Atoi(instruction[spaceIndex+2:])
		if err != nil {
			fmt.Println("Error parsing amount")
		}

		switch call {
		case "jmp":
			if incOrDec == "+" {
				i += amount
			} else {
				i -= amount
			}
		case "acc":
			if incOrDec == "+" {
				accumulator += amount
			} else {
				accumulator -= amount
			}
			i++
		case "nop":
			i++
			continue
		}
	}
	fmt.Printf("Part 1: %d\n", accumulator)
}

func followInstructions2(input []string) {
	accumulator := 0
	i := 0
	changedIndex := make(map[int]bool)
Outer:
	for {
		seen := make(map[int]bool, len(input))
		hasChangedIndex := false
		//fmt.Println("------")
		for {
			if i > len(input)-1 {
				break Outer
			}
			if seen[i] {
				break
			}
			seen[i] = true
			instruction := input[i]

			spaceIndex := strings.Index(instruction, " ")
			call := instruction[:spaceIndex]
			if (call == "jmp" || call == "nop") && !hasChangedIndex && !changedIndex[i] {
				hasChangedIndex = true
				changedIndex[i] = true
				if call == "jmp" {
					call = "nop"
				} else {
					call = "jmp"
				}
			}
			incOrDec := instruction[spaceIndex+1 : spaceIndex+2]
			amount, err := strconv.Atoi(instruction[spaceIndex+2:])
			if err != nil {
				fmt.Println("Error parsing amount")
			}
			//fmt.Printf("%s|%s|%d\n", call,incOrDec,amount)

			switch call {
			case "jmp":
				if amount == 0 {
					break
				}
				if incOrDec == "+" {
					i += amount
				} else {
					i -= amount
				}
			case "acc":
				if incOrDec == "+" {
					accumulator += amount
				} else {
					accumulator -= amount
				}
				i++
			case "nop":
				i++
			}
		}
		//fmt.Printf("Acc: %d, i: %d\n", accumulator, i)
		accumulator = 0
		i = 0
	}
	fmt.Printf("Part 2: %d\n", accumulator)
}

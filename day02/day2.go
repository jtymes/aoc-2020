package main

import (
	"aoc-2020/aocutil"
	"fmt"
	"regexp"
	"strconv"
)

func main() {
	input := aocutil.SlurpFile("./day2/input.txt")
	countValidPasswords1(input)
	countValidPasswords2(input)
}

func countValidPasswords1(input []string) {
	valid := 0
	pattern := regexp.MustCompile("([0-9]+)-([0-9]+) ([a-z]): ([a-z]+)")
	for _, s := range input {
		pieces := pattern.FindStringSubmatch(s)
		//fmt.Printf("%q\n", pieces)
		min, _ := strconv.Atoi(pieces[1])
		max, _ := strconv.Atoi(pieces[2])
		count := 0
		needle := pieces[3]
		haystack := pieces[4]

		for _, c := range haystack {
			if string(c) == needle {
				count++
			}
		}
		if count >= min && count <= max {
			valid++
		}
	}
	fmt.Printf("Part 1: %d\n", valid)
}

func countValidPasswords2(input []string) {
	valid := 0
	pattern := regexp.MustCompile("([0-9]+)-([0-9]+) ([a-z]): ([a-z]+)")
	for _, s := range input {
		pieces := pattern.FindStringSubmatch(s)
		//fmt.Printf("%q\n", pieces)
		index1, _ := strconv.Atoi(pieces[1])
		index2, _ := strconv.Atoi(pieces[2])
		count := 0
		needle := pieces[3]
		haystack := pieces[4]

		if string(haystack[index1-1]) == needle {
			count++
		}

		if string(haystack[index2-1]) == needle {
			count++
		}

		if count == 1 {
			valid++
		}
	}
	fmt.Printf("Part 2: %d\n", valid)
}

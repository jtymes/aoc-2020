package main

import (
	"aoc-2020/aocutil"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	input := aocutil.SlurpFile("./day7/input.txt")

	countBags(input)
}

type Bag struct {
	bags   []*Bag
	color  string
	counts map[string]int
}

func countBags(input []string) {
	pattern1 := regexp.MustCompile("([a-z ]+) bags?")
	pattern2 := regexp.MustCompile("([0-9]+) ([a-z ]+) bags?")
	bags := make(map[string]*Bag)

	for _, rule := range input {
		split := strings.Split(rule, "contain")
		matches := pattern1.FindAllStringSubmatch(split[0], -1)
		if len(matches) > 0 && len(matches[0]) > 1 {
			match := matches[0]
			bagColor := match[1]
			bag := getOrAddBag(bags, bagColor)

			containedBags := strings.Split(split[1], ", ")
			for _, b := range containedBags {
				parsedBag := pattern2.FindAllStringSubmatch(b, -1)
				if len(parsedBag) > 0 {
					numBags, err := strconv.Atoi(parsedBag[0][1])
					if err != nil {
						fmt.Println("Error parsing number of bags")
					}

					containedBagColor := parsedBag[0][2]
					bag.counts[containedBagColor] = numBags
					containedBag := getOrAddBag(bags, containedBagColor)

					if !bag.Contains(containedBag) {
						bag.bags = append(bag.bags, containedBag)
					}
				}

			}
		}
	}

	for k, v := range bags {
		fmt.Println(k + ":")
		for _, b := range v.bags {
			fmt.Printf("\t%d %s bags\n", v.counts[b.color], b.color)
		}
	}

	count := 0
	for _, v := range bags {
		if v.color != "shiny gold" && v.Contains(&Bag{color: "shiny gold"}) {
			count++
		}
	}
	fmt.Println(count)

	fmt.Println(bagCount(bags, bags["shiny gold"]))
}

func getOrAddBag(bags map[string]*Bag, color string) *Bag {
	bag, ok := bags[color]
	if !ok {
		bag = &Bag{
			color:  color,
			bags:   []*Bag{},
			counts: make(map[string]int),
		}
		bags[color] = bag
		return bag
	}
	return bag
}

func (b *Bag) Contains(bag *Bag) bool {
	if b.color == bag.color {
		return true
	}
	for _, innerBag := range b.bags {
		if innerBag.Contains(bag) {
			return true
		}
	}
	return false
}

func bagCount(bags map[string]*Bag, bag *Bag) int {
	thisBagCount := 0
	for color, count := range bag.counts {
		thisBagCount += count
		thisBagCount += count * bagCount(bags, bags[color])
	}
	return thisBagCount
}
